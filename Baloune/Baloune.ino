// include the library code:
#include <LiquidCrystal.h>

/****************************************************************************************************/
// Project pins mapping
/****************************************************************************************************/

int const RS = 12, EN = 11, D4 = 5, D5 = 4, D6 = 3, D7 = 2;
LiquidCrystal lcd(RS, EN, D4, D5, D6, D7);

int const LCD_BL = 6;                       // LCD backlight pin, set HIGH to light it
int const RELAY = 7;
int const SENSOR_BALOUNE_TOP = A0;          // This sensor give baloune top temperature
int const SENSOR_BALOUNE_BOTTOM = A1;       // This sensor give baloune top temperature
int const SENSOR_CAPTOR = A2;               // This sensor give captor temperature

typedef enum{NONE, LEFT, UP, RIGHT, DOWN} BUTTON_VAL;   // Button value when a key is pressed, or NONE.

BUTTON_VAL button = NONE;                               // 4 buttons are wired on an analog input

/****************************************************************************************************/
// Variables definition
/****************************************************************************************************/
int toto = 0;

int selecteur=0;
int touche=0;
int opened=0;

int d_enclenchement=8;
int d_declenchement=2;
int d_surchauffe=10;
int d_minimum=4;
int temps_remplissage=10;
int puissance_pompe=0;
int puissance_mini=30;

int timing=0;

int T_bas=0;
int T_haut=0;
int T_capt=0;

int cycle=8000;
int stepe=0;
int etat=0;


boolean bleft=false;
boolean bright=false;
boolean bmore=false;
boolean bless=false;

float ratio=1.0;

void setup() {

/****************************************************************************************************/
// Pins initialization
/****************************************************************************************************/
  pinMode(LCD_BL, OUTPUT);
  digitalWrite(LCD_BL, HIGH);                           // switch on the LCD backlight

  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, LOW);                             // Turn OFF the relay
  
  pinMode(32, OUTPUT);
  pinMode(40, OUTPUT);
  pinMode(SENSOR_BALOUNE_TOP, INPUT);
  pinMode(SENSOR_BALOUNE_BOTTOM, INPUT);
  pinMode(SENSOR_CAPTOR, INPUT);
  digitalWrite(40,LOW);
  Serial.begin(9600);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.clear();
  delay(100);
  lcd.setCursor(0, 0);
  lcd.print("Baloune V0.2.0");
  lcd.setCursor(0, 1);
  lcd.print("Initialisation..");
  delay(1000);
  //lcd.clear();
}

void loop() {
  
  
  // capture des valeurs
  // logique
  cycle+=10;
  if(cycle==9500){
    digitalWrite(40,HIGH);
  }
  Serial.println(analogRead(SENSOR_BALOUNE_TOP));
  if(cycle>=10000){
    cycle=0;
     T_bas=(analogRead(SENSOR_BALOUNE_TOP)-410)/.91;
     T_haut=(analogRead(SENSOR_BALOUNE_BOTTOM)-410)/.91;
     T_capt=(analogRead(SENSOR_CAPTOR)-410)/.91;
     digitalWrite(40,HIGH);
     opened=1;
  }
  
    //Serial.println(String(digitalRead(22)));
    // action de lecture sondes puis tests

   // routine 2
   if(etat==2){
    if(cycle==0){
      if(T_capt>(T_haut+d_surchauffe)){
        puissance_pompe+=10;
        puissance_pompe=min(puissance_pompe,100);
      }
   }else if(T_capt<(T_haut-d_minimum)){
        puissance_pompe-=10;
        puissance_pompe=max(puissance_pompe,puissance_mini);
   }
   if(T_capt<d_declenchement){
      puissance_pompe=0;
      etat=0;
   }
   }
   
    // routine 1
    if(etat==1){
      timing--;
      if(timing<=0){
        puissance_pompe=puissance_mini;
        etat=2;
      }
    }

    // routine 0
    if(T_capt>(T_bas+d_enclenchement) && etat==0){
      puissance_pompe=100;
      timing=temps_remplissage*100;
      etat=1;
    }
  //}

  // gestion de la pompe
  stepe+=10;//10;
  if(stepe>=100){
    stepe=0;
  }
  if(etat==0){
    digitalWrite(32,LOW);
    opened=0;
  }else if(etat==1){
    digitalWrite(32,HIGH);
    opened=1;
  }else{
    //digitalWrite(32,LOW);
    if(stepe>=puissance_pompe){
      if(opened==1){
      digitalWrite(32,LOW);
      opened=0;
      }
    }else{
      if(opened==0){
      digitalWrite(32,HIGH);
      opened=1;
      }
    }
  }
  

  if(bleft==true){
    if(digitalRead(22)==HIGH){
      bleft=false;
      Serial.println("LEFT OFF");
    }
  }else{
    if(digitalRead(22)==LOW && touche<0){
      Serial.println("LEFT ON");
      touche=5;
      bleft=true;
      if(selecteur>0){
        selecteur--;
      }
    }
  }

touche--;
  // boutons choix du parametre à editer
  if(bright==true){
    if(digitalRead(24)==HIGH){
      bright=false;
    }
  }else{
    if(digitalRead(24)==LOW && touche<0){
      bright=true;
      touche=5;
      if(selecteur<6){
        selecteur++;
      }
    }
  }

  // boutons edition de valeur
  if(bmore==true){
    if(digitalRead(28)==HIGH){
      bmore=false;
    }
  }else if(digitalRead(28)==LOW && touche<0){
    bmore=true;
    touche=5;
    // increment
    switch(selecteur){
      case 0:
        d_enclenchement++;
        d_enclenchement=min(100,d_enclenchement);
        break;
      case 1:
        d_declenchement++;
        d_declenchement=min(100,d_declenchement);
        break;
      case 2:
        d_surchauffe++;
        d_surchauffe=min(100,d_surchauffe);
        break;
      case 3:
        d_minimum++;
        d_minimum=min(100,d_minimum);
        break;
      case 4:
        temps_remplissage++;
        temps_remplissage=min(10,temps_remplissage);
        break;
      case 5:
        puissance_pompe++;
        puissance_pompe=min(100,puissance_pompe);
        break;
      case 6:
        puissance_mini++;
        puissance_mini=min(100,puissance_mini);
        break;
    }
  }

  if(bless==true){
    if(digitalRead(26)==HIGH){
      bless=false;
    }
  }else if(digitalRead(26)==LOW && touche<0){
    bless=true;
    touche=5;
    // increment
    switch(selecteur){
      case 0:
        d_enclenchement--;
        d_enclenchement=max(0,d_enclenchement);
        break;
      case 1:
        d_declenchement--;
        d_declenchement=max(0,d_declenchement);
        break;
      case 2:
        d_surchauffe--;
        d_surchauffe=max(0,d_surchauffe);
        break;
      case 3:
        d_minimum--;
        d_minimum=max(0,d_minimum);
        break;
      case 4:
        temps_remplissage--;
        temps_remplissage=max(0,temps_remplissage);
        break;
      case 5:
        puissance_pompe--;
        puissance_pompe=max(0,puissance_pompe);
        break;
      case 6:
        puissance_mini--;
        puissance_mini=max(0,puissance_mini);
        break;
    }
  }

  // affichage
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(""+String(etat)+":");
  switch(selecteur){
    case 0:
      lcd.print("D encl : "+String(d_enclenchement)+"");
      break;
    case 1:
      lcd.print("D decl : "+String(d_declenchement)+"");
      break;
    case 2:
      lcd.print("D surch: "+String(d_surchauffe)+"");
      break;
    case 4:
      lcd.print("tps rempl : "+String(temps_remplissage)+"");
      break;
    case 3:
      lcd.print("D mini : "+String(d_minimum)+"");
      break;
    case 5:
      lcd.print("Pow pomp : "+String(puissance_pompe));
      break;
    case 6:
      lcd.print("Pow mini : "+String(puissance_mini));
      break;
  }
  lcd.setCursor(0, 1);
  //lcd.print(""+String(d_enclenchement)+" "+String(d_declenchement)+" "+String(d_surchauffe)+" "+String(d_minimum)+" "+String(puissance_pompe)+" "+String(puissance_mini)+" "+String(T_bas)+" "+String(T_haut)+" "+String(T_capt));
  lcd.print("b"+String(T_bas)+" h"+String(T_haut)+" C"+String(T_capt)+" P"+String(puissance_pompe));
  // controle de temps : centieme de seconde
  delay(10);
}
