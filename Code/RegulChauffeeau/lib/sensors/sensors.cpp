#include "Arduino.h"
#include "sensors.h"

#define TEMPO_1HZ 1000 // 1000 ms = 1s

static uint16_t tempo;

//fonction pour convertir la valeur lu par les sondes en température
int conversionsonde(int valeur)
{
    valeur = (valeur - 409) * 100 / 87;
    return valeur;
}

void _sensorsInit(regul_t reg)
{
      //les broches d'alim des sondes

  pinMode(alimSondeCapteur, OUTPUT);
  pinMode(alimSondeBasBallon, OUTPUT);
  pinMode(alimSondeHautBallon, OUTPUT);

  //les broches de lecture des sondes

  pinMode(brocheSondeCapteur, INPUT);
  pinMode(brocheSondeBasBallon, INPUT);
  pinMode(brocheSondeHautBallon, INPUT);

}

void _sensorsRun(regul_t reg)
{

    if (tempo)
        return; // si on est supérieur à 0, on ne fait que décompté

    //les sondes pt1000 sont sur des pont diviseur (a la massse)
    //on alimente les sonde via des sortie de l'arduino uniquement pour la lecturepour limiter l'auto-échauffement
    //Résiatnce en tête de pont : marron vert rouge : 1,5k
    // A 0°C pt1000 1000 ohh Tension : 5*1000*2500= 2 V Valeur numérique : 2*1024/5 = 409
    // A 100°C pt1000 1385 ohm Tension : 5*1385/(1385+1500)= 5*1385/2855 2,40 Valeur numérique  = 2.42*1024/5 = 496
    // Temperature = (Valeurnum-409)*100/87

    //on allume la lecture des sondes

    digitalWrite(alimSondeCapteur, HIGH);
    digitalWrite(alimSondeBasBallon, HIGH);
    digitalWrite(alimSondeHautBallon, HIGH);
    //delay(20) ;
    // on lit les valeurs
    reg.tempCapteur = conversionsonde(analogRead(brocheSondeCapteur));
    reg.tempBasBallon = conversionsonde(analogRead(brocheSondeBasBallon));
    reg.tempHautBallon = conversionsonde(analogRead(brocheSondeHautBallon));
    // on éteint les sondes
    digitalWrite(alimSondeCapteur, LOW);
    digitalWrite(alimSondeBasBallon, LOW);
    digitalWrite(alimSondeHautBallon, LOW);
}

void _sensorsTimer()
{
    if (tempo){
        tempo--;
        return;
    }        

    tempo = TEMPO_1HZ;
}