#include "outputs.h"

#define TEMPO_1HZ 1000 // 1000 ms = 1s

static uint16_t tempo;

void _outputsInit(regul_t reg)
{
      pinMode(brocherelais, OUTPUT);
}

void _outputsRun(regul_t reg)
{
      if (tempo)
            return;

      // sinon fait des trucs
}

void _outputsTimer()
{
      if (tempo)
      {
            tempo--;
            return;
      }

      tempo = TEMPO_1HZ;
}