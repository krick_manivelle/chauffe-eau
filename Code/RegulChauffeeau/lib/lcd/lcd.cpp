#include "lcd.h"
#include <LiquidCrystal_I2C.h>

#define TEMPO_1HZ 1000 // 1000 ms = 1s

static uint16_t tempo;

LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

void _lcdInit(regul_t reg){

  lcd.init();      // initialize the lcd
  lcd.backlight(); //allume le retro eclairage
  lcd.clear();

  // todo afficher la super animation de démarrage

}

void _lcdRun(regul_t reg){
      if (tempo)
            return;

      // sinon fait des trucs

}

void _lcdTimer(){
    if (tempo){
        tempo--;
        return;
    }        

    tempo = TEMPO_1HZ;
}