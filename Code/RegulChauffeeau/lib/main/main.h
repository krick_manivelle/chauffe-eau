

#ifndef __MAIN_H
#define __MAIN_H

#include "Arduino.h"

// definition des broches
#define alimSondeCapteur 2
#define alimSondeBasBallon 3
#define alimSondeHautBallon 4 
#define brocheSondeCapteur A0
#define brocheSondeBasBallon A1
#define brocheSondeHautBallon A2
#define brocherelais 5
#define brochebouton A3 


// Définit les différentes étapes de la régulation
typedef enum etape_t{   MAINTENANCE,    // Arrêt pour maintenance
                        MARCHE_FORCE,   // Marche forcée du circulateur
                        REMPLISSAGE,    // Pompe à fond pour remplir le circuit car circuit autovidangeable
                        REGUL,          // Fonctionnement de la régulation solaire après remplissage
                        STOP            // Arrêt de la circulation
                         };

// Définit les différents écrans du LCD
typedef enum ecran_t{   VIDE,           // clear ecran
                        ACCEUIL,        // Ecran d'acceuil 
                        TOTO};          // rien

typedef struct regul_t{
    int tempHautBallon;
    int tempBasBallon;
    int tempCapteur;
    etape_t etape;
    ecran_t ecran;
    unsigned int pwm;

    // ajouter les paramêtres à charger depuis l'EEPROM
}regul_t;


#endif //__MAIN_H