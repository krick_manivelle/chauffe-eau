//inclusion des librairies
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include "main.h"
#include "buttons.h"
#include "eeprom.h"
#include "lcd.h"
#include "outputs.h"
#include "sensors.h"
#include "timers.h"

//declaration du lcd

//LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

// initialisation des varaibles

static regul_t regul;

void setup()
{

  //initialisation de la liason serie
  Serial.begin(9600);

  //initialisation des broches






  // initialisation des modules

  _eepromInit(regul);    // on charge les paramètres stockés dans l'eeprom
  _buttonsInit(regul);   // on initialise les boutons
  _lcdInit(regul);       // Quand beubeu l'aura fait on aura une super anim'
  _outputsInit(regul);   // ...
  _sensorsInit(regul);
  _timersInit(regul);

}

void loop()
{


  _buttonsRun(regul);
  _eepromRun(regul);
  _lcdRun(regul);
  _outputsRun(regul);
  _sensorsRun(regul);


  // lecturesonde(sonde);
  // bouton = lirebouton();

  // /*lcd.setCursor(0,0); // positionne le curseur à la colonne 1 et à la ligne 2 
  // lcd.print("capteur: ");
  // lcd.print(sonde[0]); 
  // lcd.setCursor(0,1); // positionne le curseur à la colonne 1 et à la ligne 2 
  // lcd.print("bas: ");
  // lcd.print(sonde[1]); 
  // lcd.print(" haut: ");
  // lcd.print(sonde[2]);
  // lcd.print(relais) ;*/
  // lcd.print(bouton);
  // if (relais == 0)
  // {
  //   relais = 1;
  // }
  // else
  // {
  //   relais = 0;
  // }
  // //digitalWrite(brocherelais,relais) ;

  // delay(1000);
}
